#ifndef PROPERTY_H
#define PROPERTY_H

#include <string>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

typedef enum {
  PROPERTY_UNKNOWN,
  PROPERTY_BOOL,
  PROPERTY_INT,
  PROPERTY_FLOAT,
  PROPERTY_STRING,
  PROPERTY_VECINT,
  PROPERTY_VECSTRING
} PropType;

class Property {
 protected:
  PropType type;
 public:
  Property() { type = PROPERTY_UNKNOWN; }
  Property(PropType t) { type = t; }
  PropType getType() { return type;}
  virtual ~Property() { }
  virtual string toString() { return ""; }
  virtual void parseString(string ) {};
};

class StringProperty:public Property {
 public:
  string value;
 StringProperty(const char* s): Property(PROPERTY_STRING) {
    this->value = string(s);
  }
 StringProperty(string  s): Property(PROPERTY_STRING) {
    this->value = s;
  }
  virtual void set(string s) { this->value = s; }
  virtual string toString() { return value; }
  virtual void parseString(string s) { this->value = s; }
};


class BoolProperty:public Property {
 public:
  bool value;
 BoolProperty(bool v): Property(PROPERTY_BOOL) {
    this->value = v;
  }
 BoolProperty(BoolProperty &p): Property(PROPERTY_BOOL) {
    this->value = p.value;
  }
  virtual void set(bool value) { this->value = value; }
  virtual string toString() {
    return string(value? "true":"false");
  }
  virtual void parseString(string s) { this->value = (s=="true"); }
};

class IntProperty:public Property {
 public:
  int value;
 IntProperty(int v): Property(PROPERTY_INT) {
    this->value = v;
  }
  IntProperty(IntProperty &p): Property(PROPERTY_INT) {
    this->value = p.value;
  }
  void set(int value) { this->value = value;}
  virtual string toString() {
    char str[200];
    sprintf(str, "%d", value);
    return string(str);
  }
  virtual void parseString(string s) { this->value = atoi(s.c_str()); }
};


class VecIntProperty:public Property {
 public:
  vector<int> value;
 VecIntProperty(vector<int> v): Property(PROPERTY_VECINT) {
    this->value = v;
  }
  VecIntProperty(VecIntProperty &p): Property(PROPERTY_VECINT) {
    this->value = p.value;
  }
  void set(vector<int> value) { this->value = value;}

  //TODO:: both need to be written
  virtual string toString() {
    /*char str[200];
    sprintf(str, "%d", value);
    return string(str);*/
  }
  virtual void parseString(string s) { /*this->value = atoi(s.c_str()); */ }
};


class VecStringProperty:public Property {
 public:
  vector<string> value;
 VecStringProperty(vector<string> v): Property(PROPERTY_VECSTRING) {
    this->value = v;
  }
  VecStringProperty(VecStringProperty &p): Property(PROPERTY_VECSTRING) {
    this->value = p.value;
  }
  void set(vector<string> value) { this->value = value;}

  //TODO:: both need to be written
  virtual string toString() {
    /*char str[200];
    sprintf(str, "%d", value);
    return string(str);*/
  }
  virtual void parseString(string s) { /*this->value = atoi(s.c_str()); */ }
};

class FloatProperty:public Property {
 public:
  float value;
 FloatProperty(float v): Property(PROPERTY_FLOAT) {
    this->value = v;
  }
 FloatProperty(FloatProperty &p): Property(PROPERTY_FLOAT) {
    this->value = p.value;
  }
  void set(float value) { this->value = value;}
  virtual string toString() {
    char str[200];
    sprintf(str, "%f", value);
    return string(str);
  }
  virtual void parseString(string s) { this->value = atof(s.c_str()); }
};




#endif // PROPERTY_H
