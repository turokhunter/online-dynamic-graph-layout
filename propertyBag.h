/**************************************************************************
  NetZen :  Network Sensitivity Analysis and Visualization
                             -------------------
    copyright            : (C) 2009 by Carlos D. Correa
    email                : correac@cs.ucdavis.edu
***************************************************************************/
#ifndef PROPERTY_BAG
#define PROPERTY_BAG

#include <map>
#include <vector>
#include "object.h"
#include "property.h"

using namespace std;

class PropertyBag : public Object {
    friend class DataControl2;
 protected:
  map<string, Property*> properties;
 public:
  PropertyBag(const char*type, const char *name);
  virtual ~PropertyBag();
  vector<string> getProperties();
  bool hasProperty(string name);
  void setProperty(string name, Property *p);
  void setProperty(string name, string value);
  Property* getProperty(string name);
 
  void setPropertyString(string name, string s);
  void setPropertyString(string name, const char * s);

  void setPropertyFloat(string name, float v);
  void setPropertyInt(string name, int v);
  void setPropertyBool(string name, bool v);
  void setPropertyVecInt(string name, vector<int> v);
  void setPropertyVecString(string name, vector<string> v);


  /*
  void setPropertyVec2(string name, float x, float y);
  void setPropertyVec2v(string name, float *v);
  void setPropertyVec3(string name, float x, float y, float z);
  void setPropertyVec3v(string name, float *v);
  */
  string getPropertyString(string name);
  int getPropertyInt(string name, int defaultValue = 0);
  bool getPropertyBool(string name, bool defaultValue = false);
  float getPropertyFloat(string name, float defaultValue = 0.0);
  vector<int> & getPropertyVecInt(string name);
  vector<string> & getPropertyVecString(string name);

  /*
  void getPropertyVec2(string name, float *v);
  void getPropertyVec3(string name, float *v);
  */
  int getPropertyType(string name);
 
};

#endif
