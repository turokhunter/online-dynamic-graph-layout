/*******************************************************************************
                          GM3 - GPU implementation of FM3
                            ----------------------------
                          Copyright: (C) 2011 by Nick Leaf
                              Email: njleaf@ucdavis.edu
*******************************************************************************/

/***************************************************************************
 *                                                                         *
 * Copyright: All rights reserved.  May not be used, modified, or copied   *
 * without permission.                                                     *
 *                                                                         *
 ***************************************************************************/

//KD tree node structure
// - mirrors kdnode definition found in GPULayout.h

float rep(float C, float2 r);
float nodeRep(float C, __global float2 *nodep);
float edgeForce(int n, float2 p, __global float2 *nodep, __global int *CSRN, __global int *CSRE, __global float *edgew, float desLength);

float rep(float C, float2 r){
    //return max(w1 * w2, 1.f) * (C / pow(dot(r,r) + eta, 1.5f)) * r;
    //return max(w1 * w2, 1.f) * (C / (length(r) + eta));//Mine
    return (C /length(r));//Mine
    //return max(w1 * w2, 1.f) *0.5*0.1*0.1*log((length(r)*length(r) + eta));//frisham
}


float nodeRep(float C, __global float2 *nodep)
{
    uint nID = get_local_id(0);
    float f = 0.f;
    float2 r = 0.f;
    uint i;

    uint ns = get_global_size(0);

    for(i = 0; i < ns; i++){
        r = nodep[nID] - nodep[i];
        if(any(isnotequal(nodep[nID], nodep[i]))){//p[lID].x == p[i].x && p[lID].y == p[i].y){
          f = f + rep(C, r);
        }

    }


    return f;
}


float edgeForce(int n,
                 float2 p,
                 __global float2 *nodep,
                 __global int *CSRN,
                 __global int *CSRE,
                 __global float *edgew,
                 float desLength)
{
    int s = CSRN[n];
    int t = CSRN[n+1];

    float f = 0.f;
    float2 r = 1.f;
    float d = 1.f;
    float w = 1.f;

    for(int e = s; e < t; e++){
        r = p - nodep[CSRE[e]];
        d = length(r);
        w = edgew[e];
        //f -= max(w, 1.f) * d * log((d+eta) / desLength) * r;
        //f += max(w, 1.f) *pown(d,3)/3*(log(d/desLength) - 1/3);//Mine
        //f -= 1/.3*pown(d,3);//Frishman
        //f += max(w, 1.f) * 1/9.0 * ( pown(d,3) * (3*log(d/desLength) - 1) + pown(desLength,3)); //Nick
          f += 1/9.0 * ( pown(d,3) * (3*log(d/desLength) - 1) + pown(desLength,3)); //Nick


    }

    return f;
}



__kernel void potentialKern(
          /* 0  */ __global float2 *nodep,
          /* 1  */ __global int *CSRN,
          /* 2  */ __global int *CSRE,
          /* 3  */ __global float *nodew,
          /* 4  */ __global float *edgew,
          /* 5  */ __global float *results,
          /* 6  */ float C, //repulsive constant
          /* 7  */ float desLength) //desired spring length
{


    uint nID = get_global_id(0);

    float f = 0.f;

    f += nodeRep(C, nodep);
    f += edgeForce(nID, nodep[nID], nodep, CSRN, CSRE, edgew, desLength);
    results[nID] = f;



    
}
