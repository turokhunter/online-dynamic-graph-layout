#include <stdio.h>
#include <stdlib.h>
#include <QString>

#include "Layout/GM3Interface.h"
#include "node-edge.h"
#include "datacontrol.h"


int main(int argc, char *argv[])
{
  if(argc != 3){
    printf("LayoutCode folder filename\n");
    exit(1);
   }

   const int numlines = 10;
   const int numiterations = 3;

  //holds graph and loader
  DataControl * data = new DataControl();

  //loads the data. An example file is located in "example" folder
  data->load(QString::fromStdString(argv[1]));


  //initial calculation step
  computeLayout(data->getGraph());

  //write initial file
  string filename = argv[2];

  data->writeToFile(filename + "_0000.txt" );


  /*GM3::Parameterization hold all the information on default
  parameters and allows varaible to be changed as needed*/

  GM3::Parameterization *p = new GM3::Parameterization();
  p->incremental = true;

  int filenum = 1;
  char str[6];
  bool read = false;

  //Keeps reading until there is no more data left
  while(1){
      read = data->readMore(numlines);
      if(!read)
        break;

      //compute an incremental layout
      computeLayout(data->getGraph(), p);

      //Determines which nodes need to readjust and moves them
      for(int i = 0 ; i < numiterations; i++)
        computeOneRefinementStep(data->getGraph());

      sprintf(str,"%04d", filenum);
      data->writeToFile(filename + "_"+str+".txt");

      filenum++;
  }

  return 0;
}
