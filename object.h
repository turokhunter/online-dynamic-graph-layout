#ifndef OBJECT_H
#define OBJECT_H

#include <stdio.h>
#include <stdarg.h>
#include <string>
using namespace std;

class Object {
  string name;  
  string type;  
 public:
  Object(const char *t, const char *n) {
    type = string(t);
    name = string(n);
  }
  virtual ~Object() {}
  void error(const char *msg, ...) {
    va_list args;
    va_start(args, msg);
    fprintf(stderr, "[%s.%s] ERROR ", type.c_str(), name.c_str());
    vfprintf(stderr, msg, args);
    va_end(args);
  }

  void print(const char *msg, ...) {
    va_list args;
    va_start(args, msg);

    
    fprintf(stdout, "[%s.%s] ", type.c_str(), name.c_str());
    vfprintf(stdout, msg, args);
    va_end(args);
  }

  string getName() {
    return name;
  }
  string getType() {
    return type;
  }
};


#endif
