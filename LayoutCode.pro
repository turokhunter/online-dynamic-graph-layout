#-------------------------------------------------
#
# Project created by QtCreator 2015-10-18T15:50:33
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = LayoutCode
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11

TEMPLATE = app

mac {
  CONFIG -= app_bundle
  LIBS += -framework OpenCL
  QMAKE_LFLAGS += -framework OpenCL

} else { #assume linux
  LIBS += -lGLU -lOpenCL -lGLEW
}

SOURCES += main.cpp \
           layout/*.cpp \
           datacontrol.cpp \
           propertyBag.cpp

HEADERS += node-edge.h \
          layout/*.h \
          datacontrol.h \
          propertyBag.h \
          property.h \
          object.h

